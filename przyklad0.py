import sys


nasza_lista = []
for x in range(70_000_000):
    nasza_lista.append(x**2)

x = [x**2 for x in range(70_000_000)]

print(type([x**2 for x in range(70_000_000)]))

print(type((x**2 for x in range(70_000_000))))

print(sys.getsizeof([x**2 for x in range(70_000_000)])) # ponad pół giga

print(sys.getsizeof((x**2 for x in range(70_000_000))))

x = (x**2 for x in range(70_000_000))

for z in [x**2 for x in range(70_000_000)]:
    print(z)

for z in (x**2 for x in range(70_000_000)):
    print(z)

%time func()