krotka1 = (5, 4, 4, -1)
krotka2 = (5, 4 ,6, 7)

#Czy suma odpowiadajacych elementow = 16 ?

# for x, y in zip((x * 2 for x in krotka1),(x * 2 for x in krotka2)):
#     if x + y == 16:
#         break


print(any([True for x, y in zip((x * 2 for x in krotka1),(x * 2 for x in krotka2)) if x + y == 16]))

print(any([lambda x,y: x + y == 16 for x, y in zip((x * 2 for x in krotka1),(x * 2 for x in krotka2))]))

print(any([x + y == 16 for x, y in zip((x * 2 for x in krotka1),(x * 2 for x in krotka2))]))



print(any(x + y == 16 for x, y in zip((x * 2 for x in krotka1),(x * 2 for x in krotka2))))

#--------------------------------------

def all(iterable):
    for element in iterable:
        if not element:
            return False
    return True

# any(iterable)

# Return True if any element of the iterable is true. If the iterable is empty, return False. Equivalent to:

# def any(iterable):
#     for element in iterable:
#         if element:
#             return True
#     return False

