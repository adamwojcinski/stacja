#http://getitjob.pl/2019/11/06/akcja-rekrutacja-python-25/

krotka1 = (3, 4, 5)
krotka2 = (1, 2, 3, 4, 5, 6, 7, 8, 9)
def czy_krotka_w_krotce(krotka1, krotka2):
    return all(
        element in krotka2
        for element in krotka1
    )
print(czy_krotka_w_krotce(krotka1, krotka2))