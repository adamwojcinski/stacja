import string
import itertools

letters = string.ascii_lowercase

real_password = tuple('karol')

for i, password in enumerate(itertools.product(letters, repeat=5)):
    if password == real_password:
        print(f'The password is : {"".join(real_password)}')
        print(f'{i} failures')
        break
