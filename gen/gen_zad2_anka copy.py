def gen_liczb_naturalnych():
    a = 0
    while True:
        a += 1
        yield a

def gen_paczek(n):
    liczby_naturalne = gen_liczb_naturalnych()
    #lista = []
    while True:
        # for _ in range(n):
        #     lista.append(next(liczby_naturalne))
        yield [next(liczby_naturalne) for _ in range(n)]
        #lista.clear()

paczki_liczby_naturalne = gen_paczek(3)
print(next(paczki_liczby_naturalne))
print(next(paczki_liczby_naturalne))
print(next(paczki_liczby_naturalne))