#http://getitjob.pl/2019/07/15/akcja-rekrutacja-python-4/

def gen_liczb_naturalnych():
    a = 0
    while True:
        a += 1
        yield a

def gen_list_liczb_naturalnych(n):
    liczby_naturalne = gen_liczb_naturalnych()
    lista = []
    while True:
        for _ in range(n):
            lista.append(next(liczby_naturalne))
        yield lista
        lista.clear()
    
listy_liczb_nat = gen_list_liczb_naturalnych(3)
print(next(listy_liczb_nat))
print(next(listy_liczb_nat))
print(next(listy_liczb_nat))