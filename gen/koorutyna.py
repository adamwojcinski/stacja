def gen():
    a = 0
    x = 1
    while x != 0:
        x = yield a
        a += 1

nasz_gen = gen()
next(nasz_gen)
print(nasz_gen.send((1,2,3)))
print(nasz_gen.send(1))
#print(nasz_gen.send(0))