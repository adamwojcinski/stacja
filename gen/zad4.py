lista = [1, 2, 3, 4, 5 ,6]

# def gen(lista):
#     lista = iter(lista)
#     while True:
#         try:
#             yield next(lista), next(lista)
#         except StopIteration:
#             return

def gen2(lista):
    i = 0
    while True:
        yield lista[i: i+2]
        i += 1

for x in gen2(lista):
    print(x)